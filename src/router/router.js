import App from '../App'

export default [{
    path: '/',
    component: App,
    children: [{
        path: '/home/:id',
        component: r => require.ensure([], () => r(require('../page/home')), 'home')
    }, {
        path: '/work/:id',
        component: r => require.ensure([], () => r(require('../page/work')), 'work')
    }, {
        path: '/info/:id',
        component: r => require.ensure([], () => r(require('../page/information')), 'information')
    }, {
        path: '/success/:id',
        component: r => require.ensure([], () => r(require('../page/success')), 'success')
    }, {
        path: '/downLoad/:id',
        component: r => require.ensure([], () => r(require('../page/downLoad')), 'downLoad')
    }, {
        path: ':id',
        component: r => require.ensure([], () => r(require('../page/before')), 'before')
    }]
}]
