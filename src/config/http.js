import axios from 'axios';

const http = axios.create({
    // baseURL: 'http://embatest.superong.com/v2', //设置默认api路径
    baseURL: 'http://embadev.superong.com/v2', //设置默认api路径
    timeout: 5000, //设置超时时间,
    // headers: {'contentType':'application/json; charset=utf-8'},
});
http.interceptors.response.use(function (response) {
    // Do something with response data
	if (response.status == 200) {
		return response.data;
	}

  }, function (error) {
    // Do something with response error
    return Promise.reject(error);
  });
export default http;
