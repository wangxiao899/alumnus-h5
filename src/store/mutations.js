const ADD_ROLL = 'ADD_ROLL'; //添加学籍信息
const SAVE_YEAR = 'SAVE_YEAR'; // 选择入学年份
const INIT_DATA = 'INIT_DATA'; //获取初始数据
const TAB_ACTIVE = 'TAB_ACTIVE'; // 学籍弹出层切换
const CLASS_LIST = 'CLASS_LIST';// 学籍弹出层显示列表
const SAVE_NAME = 'SAVE_NAME';//保存姓名
const SAVE_WORK = 'SAVE_WORK';//保存工作
const SAVE_TITLE = 'SAVE_TITLE';//保存职位
const SAVE_PHONE = 'SAVE_PHONE';//保存手机号
const SAVE_EMAIL = 'SAVE_EMAIL';//保存邮箱
const SAVE_CODE = 'SAVE_CODE';
const SET_ROLL = 'SET_ROLL';
const WORK_ERROR = 'WORK_ERROR';
const TITLE_ERROR = 'TITLE_ERROR';
const GET_PHONE_AREA = 'GET_PHONE_AREA';
const SELECT_PHONE_AREA = 'SELECT_PHONE_AREA';
const SET_INDEX = 'SET_INDEX';
export default {
	[ADD_ROLL](state, data) {
		state.RollData = data;
	},
	[SET_ROLL](state, data) {
		state.rolls = data;
	},
	[SAVE_YEAR](state, data) {
		state.year = data;
	},
	[INIT_DATA](state, data) {
		state.initDataList = data;
	},
	[TAB_ACTIVE](state, data) {
		state.tabActive = data;
	},
	[CLASS_LIST] (state, data) {
		state.classList = data;
	},
	[SAVE_NAME](state, data) {
		state.name = data;
	},
	[SAVE_WORK](state, data) {
		state.work = data;
		state.errorWork = false;
	},
	[SAVE_TITLE] (state, data) {
		state.title = data;
		state.errorTitle = false;
	},
	[SAVE_PHONE] (state, data) {
		state.phone = data;
	},
	[SAVE_EMAIL] (state, data) {
		state.email = data;
	},
	[SAVE_CODE] (state, data) {
		state.code = data;
	},
	[WORK_ERROR] (state, data) {
		state.errorWork = true;
	},
	[TITLE_ERROR] (state, data) {
		state.errorTitle = true;
	},
	[GET_PHONE_AREA] (state, data) {
		state.phoneArea = data;
	},
	[SELECT_PHONE_AREA] (state, data) {
		state.activePhone = data;
	},
	[SET_INDEX] (state, data) {
		state.oneIndex = data;
	}
}
