import http from '../config/http';
export default {
	addRoll({ commit, state }, data) {
			commit('ADD_ROLL', data);
	},
	setRoll({ commit, state }, data) {
			commit('SET_ROLL', data);
	},
	saveYear({ commit, state }, data) {
			commit('SAVE_YEAR', data);
	},
	toggleTab({ commit, state }, data) {
			commit('TAB_ACTIVE', data);
	},
	setClassList({ commit, state }, data) {
			commit('CLASS_LIST', data);
	},
	setIndex({ commit, state }, data) {
			commit('SET_INDEX', data);
	},
	initData({ commit, state }, data) {
        http.get(`/convenes/collegeInfo?uid=${data}`).then((response) => {
			// commit('INIT_DATA', data);
			if (response.code == 200) {
				commit('INIT_DATA', response.data);
			}
		}).catch((error) => {
			console.log(error)
		});

	},
	getPhoneArea({ commit, state }, data) {
        http.get(`/options/phoneAreaCode`).then((response) => {
			if (response.code == 200) {
				commit('GET_PHONE_AREA', response.data);
			}
		}).catch((error) => {
			console.log(error)
		});

	},
	selectPhoneArea({ commit, state }, data) {
			commit('SELECT_PHONE_AREA', data);
	},
	saveName({ commit, state }, data) {
		const val = data.target.value;
			commit('SAVE_NAME', val);
	},
	saveWork({ commit, state }, data) {
		const val = data.target.value;
			if (val.length > 100) {
				commit('WORK_ERROR', val);
				return
			}
			commit('SAVE_WORK', val);
	},
	saveTitle({ commit, state }, data) {
		const val = data.target.value;
			if (val.length > 30) {
				console.log()
				commit('TITLE_ERROR', data)
				return
			}
			commit('SAVE_TITLE', val);
	},
	savePhone({ commit, state }, data) {
		const val = data.target.value;
			commit('SAVE_PHONE', val);
	},
	saveEmail({ commit, state }, data) {
		const val = data.target.value;
			commit('SAVE_EMAIL', val);
	},
	saveCode({ commit, state }, data) {
		const val = data.target.value;
			commit('SAVE_CODE', val);
	},
	sendPhoneCode({ commit, state }, data) {
		http.post('/sms/refConvene', {phone: data.phone}).then((response) => {
			if (response.code == 200) {
				alert('短信发送成功，请注意查收！');
				data.cbk()
			}
		}).catch((error) => {
			console.log(error)
		});
	},
	subConvenes({ commit, state }, data) {
		http.post('/convenes', data.data).then((response) => {
			if (response.code == 200) {
				data.cbk(response.data);
			} else {
				alert(response.zh)
			}
		}).catch((error) => {
			console.log(error)
		});
	},


}
