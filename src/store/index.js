import Vue from 'vue'
import Vuex from 'vuex'
import mutations from './mutations'
import actions from './action'


Vue.use(Vuex)

const state = {
	RollData: {
		projects: {name:'请选择'},
		majors: {},
		addressBooks: {}
	},
	initDataList: null,
	tabActive: 1,
	classList: [],
	name: '',
	title: '',
	work: '',
	phone: '',
	email: '',
	code: '',
	rolls: null,
	errorWork: false,
	errorTitle: false,
	phoneArea: [],
	oneIndex: 0,
	activePhone: {"id":1,"name":"中国","eName":"China","shortEName":"CN","phoneCode":"+86"}
}

export default new Vuex.Store({
	state,
	actions,
	mutations
})
